from selenium import webdriver


class MainPage():
    main_page_title_text = 'My Store'

    def __init__(self, driver):
        self.driver = driver

        self.logo = "/html/body/div/div[1]/header/div[3]/div/div/div[1]/a/img"
        self.login_btn = "/html/body/div/div[1]/header/div[2]/div/div/nav/div[1]/a"


    def click_sign_in(self):
        self.driver.find_element_by_xpath(self.login_btn).click()
        return self

    def click_on_logo(self):
        self.driver.find_element_by_xpath(self.logo).click()
        return self

    def return_title(self):
        main_title = self.driver.title
        return main_title
