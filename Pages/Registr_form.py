from selenium.webdriver.common.keys import Keys

from selenium import webdriver
from Pages.Login_Page import LoginPage

class Registr_form():
    first_name = 'Alex'
    last_name = 'Artem'
    year_of_birth = 2010
    day_of_birth = 20
    month_of_birth = 'July'
    password_text = 'abrakadabra'
    company_name = 'Int'
    address_text = "Indep square"
    address_text2 = 'Khreschatyk'
    city_text = 'Kyiv'
    state_text = 'Alabama'

    def __init__(self, driver):
        # your personal info block

        self.driver = driver
        self.mr_radio_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[1]/label'
        self.mrs_radio_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[1]/div[2]/label'
        self.first_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[2]/input'
        self.last_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[3]/input'
        self.autofilled_email = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[4]/input'
        self.password = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[5]/input'
        self.date_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[1]/div/select'
        self.month_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[2]/div/select'
        self.year_of_birth = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[6]/div/div[3]/div/select'
        self.signUp_newsletter_checkbox = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[7]/div/span/input'
        self.receive_spec_offers = '/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[8]/div/span/input'

        # your address block
        self.autofilled_first_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[1]/input'
        self.autofilled_last_name = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[2]/input'
        self.company = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[3]/input'
        self.address = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[4]/input'
        self.address2 = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[5]/input'
        self.city = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[6]/input'
        self.state = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[7]/div/select'
        self.zip_code = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[8]/input'
        self.country = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[9]/div/select'
        self.add_info = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[10]/textarea'
        self.home_phone = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[12]/input'
        self.mobile_phone = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[13]/input'
        self.my_address = '/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[14]/input'
        self.register_btn = '/html/body/div/div[2]/div/div[3]/div/div/form/div[4]/button/span'

    def mr_radio_click(self):
        self.driver.find_element_by_xpath(self.mr_radio_btn).click()
        return self

    def mrs_radio_click(self):
        self.driver.find_element_by_xpath(self.mrs_radio_btn).click()
        return self

    def fill_first_name(self):
        self.driver.find_element_by_xpath(self.first_name).send_keys('Alex')
        return self

    def fill_last_name(self):
        self.driver.find_element_by_xpath(self.last_name).send_keys('Artem')
        return self

    def fill_password(self):
        self.driver.find_element_by_xpath(self.password).send_keys(LoginPage.c)
        return self

    def fill_date_of_birth(self):
        self.driver.find_element_by_xpath(self.date_of_birth).send_keys('20')
        return self

    def fill_month_of_birth(self):
        self.driver.find_element_by_xpath(self.month_of_birth).send_keys("July")
        return self

    def fill_year_of_birth(self):
        self.driver.find_element_by_xpath(self.year_of_birth).send_keys(Registr_form.year_of_birth)
        return self

    def sign_up_for_newsletter(self):
        self.driver.find_element_by_xpath(self.signUp_newsletter_checkbox).click()
        return self

    def receive_special_offers_turn_on(self):
        self.driver.find_element_by_xpath(self.receive_spec_offers).click()
        return self

    def autofilled_first_name_check(self):
        # self.driver.find_element_by_xpath(self.autofilled_first_name)
        autofilled_fname = self.driver.find_element_by_xpath(self.autofilled_first_name).get_attribute('value')
        print("autofilled first name = ", autofilled_fname)
        return autofilled_fname

    def autofilled_last_name_check(self):
        # self.driver.find_element_by_xpath(self.autofilled_last_name)
        autofilled_lname = self.driver.find_element_by_xpath(self.autofilled_last_name).get_attribute('value')
        print('autofilled last name= ', autofilled_lname)
        return autofilled_lname

    def fill_company(self):
        self.driver.find_element_by_xpath(self.company).send_keys(self.company_name)
        return self

    def fill_address(self):
        self.driver.find_element_by_xpath(self.address).send_keys(self.address_text)
        return self

    def fill_address2(self):
        self.driver.find_element_by_xpath(self.address2).send_keys(self.address_text2)
        return self

    def fill_city(self):
        self.driver.find_element_by_xpath(self.city).send_keys(self.city_text)
        return self

    def fill_state(self):
        self.driver.find_element_by_xpath(self.state).send_keys(self.state_text)
        return self

    def fill_zip_postal_code(self):
        self.driver.find_element_by_xpath(self.zip_code).send_keys('00000')
        return self

    def fill_country(self):
        self.driver.find_element_by_xpath(self.country).send_keys('United States')
        return self

    def fill_add_info(self):
        self.driver.find_element_by_xpath(self.add_info).send_keys('blah-blah-blah')
        return self

    def fill_home_phone(self):
        self.driver.find_element_by_xpath(self.home_phone).send_keys('1234567890')
        return self

    def fill_mobile_phone(self):
        self.driver.find_element_by_xpath(self.mobile_phone).send_keys('0987654321')
        return self

    def fill_my_address(self):
        self.driver.find_element_by_xpath(self.my_address).send_keys('some secret address')
        return self

    def click_register(self):
        self.driver.find_element_by_xpath(self.register_btn).click()
        return self
