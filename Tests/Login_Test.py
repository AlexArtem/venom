from selenium import webdriver

import unittest

from Pages.Login_Page import LoginPage
from Pages.MainPage import MainPage


class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="///Users/Alex/PycharmProjects/Venom/Drivers/chromedriver")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_Valid_Login(self):
        driver = self.driver

        driver.get("http://automationpractice.com/")

        sign_in = MainPage(driver)  # enter the login page from main page777
        sign_in.click_sign_in()

        login = LoginPage(driver)  # filling registered credentials
        login.enter_registered_email()
        login.enter_registered_password()
        login.registered_sign_in_click()
        #for new commit

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
        print("test completed")


if __name__ == '__main__':
    unittest.main()
