import unittest

from selenium import webdriver

from Pages.Login_Page import LoginPage
from Pages.MainPage import MainPage
from Pages.Registr_form import Registr_form


class Main_Page_Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="///Users/Alex/PycharmProjects/Venom/Drivers/chromedriver")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_main_page_title(self):
        driver = self.driver
        driver.get("http://automationpractice.com/")

        main_page = MainPage(driver)

        title = main_page.return_title()
        self.assertEqual(title, MainPage.main_page_title_text)

        main_page.click_on_logo()
        title = main_page.return_title()
        self.assertEqual(title, MainPage.main_page_title_text)

    @classmethod
    def tearDownClass(cls):
        # cls.driver.close()
        # cls.driver.quit()
        print("test completed")


if __name__ == '__main__':
    unittest.main()
