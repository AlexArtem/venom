import unittest

from selenium import webdriver

from Pages.Login_Page import LoginPage
from Pages.MainPage import MainPage
from Pages.Registr_form import Registr_form


class Registration_Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(executable_path="/Users/Alex/PycharmProjects/Venom/Drivers/chromedriver")
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_registration(self):
        driver = self.driver
        driver.get("http://automationpractice.com/")

        sign_in = MainPage(driver)  # enter the login page from main page
        sign_in.click_sign_in()

        registr = LoginPage(driver)
        registr.fill_account_email()
        registr.create_account_btn_click()

        regform = Registr_form(driver)
        regform.mr_radio_click() \
            .mrs_radio_click() \
            .fill_first_name() \
            .fill_last_name() \
            .fill_password() \
            .fill_date_of_birth() \
            .fill_month_of_birth() \
            .fill_year_of_birth() \
            .sign_up_for_newsletter() \
            .receive_special_offers_turn_on()

        autofilled_first_name = regform.autofilled_first_name_check()
        self.assertEqual(autofilled_first_name, Registr_form.first_name)

        autofilled_last_name = regform.autofilled_last_name_check()
        self.assertEqual(autofilled_last_name, Registr_form.last_name)

        regform.fill_company() \
            .fill_address() \
            .fill_address2() \
            .fill_city() \
            .fill_state() \
            .fill_zip_postal_code() \
            .fill_country() \
            .fill_add_info() \
            .fill_home_phone() \
            .fill_mobile_phone() \
            .fill_my_address() \
            .click_register()

    @classmethod
    def tearDownClass(cls):
        # cls.driver.close()
        # cls.driver.quit()
        print("test completed")
        f = open('/Users/Alex/PycharmProjects/Venom/password.txt', 'a')
        f.write('Login = ' + LoginPage.b + ' Password = ' + LoginPage.c + '\n')
        f.close()


if __name__ == '__main__':
    unittest.main()
